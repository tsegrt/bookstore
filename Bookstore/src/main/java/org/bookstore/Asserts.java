package org.bookstore;

public class Asserts {
	

	public static <T> void argumentIsNotNull(T arg, String argName) {
        if (arg == null){
        	if (argName.equals("Username") || argName.equals("Password")) {
        		throw new IllegalArgumentException(argName + " is not long enough!");
        	}
        	if (argName.equals("Card number") || argName.equals("CVV2")) {
        		throw new IllegalArgumentException(argName + " is not written in the correct format!");
        	}
        	else {
        		throw new IllegalArgumentException(argName + " cannot be null!");
        	}
        }
    }
	
}
