package org.bookstore.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.bookstore.serialization.LocalDateSerializer;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name="Books")
public class Book implements Serializable{

	private static final long serialVersionUID = 3924949255266145824L;
	
	private int id;
	private String title;
	private String author;
	private String language;
	private String cover;
	private String description;
	private BigDecimal price;
	private boolean available;
	private String publisher;
	private LocalDate releaseDate;
	private String ISBN;
	private int availableItems;
	private int soldItems;
	private Set<OrderBook> orderBooks = new HashSet<OrderBook>();
	private Set<WishListBook> wishListBooks = new HashSet<WishListBook>();
	private Category category;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_seq_gen")
	@SequenceGenerator(name = "book_seq_gen", sequenceName = "book_sequence")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(nullable=false)
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(nullable=false)
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	@Column(nullable=false, length=25)
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	@Column
	public String getCover() {
		return cover;
	}
	
	public void setCover(String cover) {
		this.cover = cover;
	}
	
	@Column(columnDefinition="Text", nullable=false)
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Min(0)
	@Column(nullable=false, precision=10, scale=2)
	public BigDecimal getPrice() {
		return price;
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	@Type(type = "true_false")
	@Column(nullable=false)
	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	@Column(nullable=false)
	public String getPublisher() {
		return publisher;
	}
	
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	@JsonSerialize(using = LocalDateSerializer.class)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@Column(nullable=false)
	public LocalDate getReleaseDate() {
		return releaseDate;
	}
	
	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}
	
	
	@Size(min=13, max=17)
	@Column(nullable=false, unique=true, length=17)
	public String getISBN() {
		return ISBN;
	}
	
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}
	
	@Min(0)
	@Column(nullable=false)
	public int getAvailableItems() {
		return availableItems;
	}
	
	public void setAvailableItems(int availableItems) {
		this.availableItems = availableItems;
	}
	
	@Min(0)
	@Column(nullable=false)
	public int getSoldItems() {
		return soldItems;
	}
	
	public void setSoldItems(int soldItems) {
		this.soldItems = soldItems;
	}
	
	
	@JsonIgnore
	@OneToMany(mappedBy="book")
	public Set<OrderBook> getOrderBooks() {
		return orderBooks;
	}

	public void setOrderBooks(Set<OrderBook> orderBooks) {
		this.orderBooks = orderBooks;
	}
	
	
	@JsonIgnore
	@OneToMany(mappedBy="book")
	public Set<WishListBook> getWishListBooks() {
		return wishListBooks;
	}

	public void setWishListBooks(Set<WishListBook> wishListBooks) {
		this.wishListBooks = wishListBooks;
	}

	@ManyToOne
	@JoinColumn(name="category_id", nullable=false)
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Book))
			return false;
		Book other = (Book) obj;
		if (getId() != other.getId())
			return false;
		if (getISBN() == null) {
			if (other.getISBN() != null)
				return false;
		}
		else if (!getISBN().equals(other.getISBN())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getId();
		result = prime * result + ((getISBN() == null) ? 0 : getISBN().hashCode());
		return result;
	}
}
