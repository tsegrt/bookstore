package org.bookstore.model;

public enum State {
	ACTIVE("Active"),
	INACTIVE("Inactive");
	
	private String state;

	private State(final String state) {
		this.state = state;
	}

	public String getState() {
		return state;
	}
	
	

}
