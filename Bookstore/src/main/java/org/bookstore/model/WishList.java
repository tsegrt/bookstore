package org.bookstore.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "WishList")
public class WishList implements Serializable{

	private static final long serialVersionUID = 2722513982504629887L;
	
	private int id;
	private Account account;
	private Set<WishListBook> wishListBooks = new HashSet<WishListBook>();
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wishlist_seq_gen")
	@SequenceGenerator(name = "wishlist_seq_gen", sequenceName = "wishlist_sequence")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@OneToOne
	@JoinColumn(name="account_id", nullable=false, unique=true)
	public Account getAccount() {
		return account;
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}


	@JsonIgnore
	@OneToMany(mappedBy="wishList", cascade=CascadeType.REMOVE)
	public Set<WishListBook> getWishListBooks() {
		return wishListBooks;
	}

	public void setWishListBooks(Set<WishListBook> wishListBooks) {
		this.wishListBooks = wishListBooks;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WishList))
			return false;
		WishList other = (WishList) obj;
		if (getId() != other.getId())
			return false;
		if (getAccount() == null) {
			if (other.getAccount() != null)
				return false;
		}
		else if (!getAccount().equals(other.getAccount())) {
			return false;
		}
		return true;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getId();
		result = prime * result + ((getAccount() == null) ? 0 : getAccount().hashCode());
		return result;
	}
	
	
	
	
	
}
