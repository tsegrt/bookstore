package org.bookstore.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.bookstore.serialization.LocalDateSerializer;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
public class PaymentOption implements Serializable{

	private static final long serialVersionUID = -4764460560774674808L;
	
	private int id;
	private String fullName;
	private String type;
	private String cardNumber;
	private String CVV2;
	private LocalDate expirationDate;
	private Order order;

	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "payment_seq_gen")
	@SequenceGenerator(name = "payment_seq_gen", sequenceName = "payment_sequence")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	@Column(nullable=false)
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	@Column(nullable=false)
	public String getCVV2() {
		return CVV2;
	}

	public void setCVV2(String cVV2) {
		CVV2 = cVV2;
	}

	
	@Column(nullable=false, length=20)
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	
	@Column(nullable=false)
	public String getCardNumber() {
		return cardNumber;
	}
	
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	
	@JsonSerialize(using = LocalDateSerializer.class)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@Column(nullable=false)
	public LocalDate getExpirationDate() {
		return expirationDate;
	}
	
	public void setExpirationDate(LocalDate expirationDate) {
		this.expirationDate = expirationDate;
	}


	@JsonIgnore
	@OneToOne
	@JoinColumn(name="order_id", nullable=false)
	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

}
