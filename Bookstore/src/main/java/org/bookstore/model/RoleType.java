package org.bookstore.model;

public enum RoleType {
	USER("USER"),
	ADMIN("ADMIN");
	
	String userRoleType;

	private RoleType(String userRoleType) {
		this.userRoleType = userRoleType;
	}

	public String getUserRoleType() {
		return userRoleType;
	}
	
	

}
