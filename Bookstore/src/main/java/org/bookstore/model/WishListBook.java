package org.bookstore.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "WishList_Books")
public class WishListBook implements Serializable {
	
	private static final long serialVersionUID = 1772813635443526204L;
	
	private int id;
	private WishList wishList;
	private Book book;
	
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wishlistbook_seq_gen")
	@SequenceGenerator(name = "wishlistbook_seq_gen", sequenceName = "wishlistbook_sequence")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	@ManyToOne
	@JoinColumn(name="book_id")
	public Book getBook() {
		return book;
	}
	
	public void setBook(Book book) {
		this.book = book;
	}
	
	
	@ManyToOne
	@JoinColumn(name="wishList_id")
	public WishList getWishList() {
		return wishList;
	}

	public void setWishList(WishList wishList) {
		this.wishList = wishList;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WishListBook))
			return false;
		WishListBook other = (WishListBook) obj;
		return (this.getId() == other.getId()) &&
			   (this.getWishList() != null && getWishList().equals(other.getWishList())) &&
			   (this.getBook() != null && getBook().equals(other.getBook()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getId();
		result = prime * result + ((getWishList() == null) ? 0 : getWishList().hashCode());
		result = prime * result + ((getBook() == null) ? 0 : getBook().hashCode());
		return result;
	}

}

