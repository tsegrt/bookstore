package org.bookstore.model;

public enum PaymentType {
	VISA("Visa"),
	MASTERCARD("MasterCard"),
	AMERICANEXPRESS("American Express"),
	MAESTRO("Maestro");
	
	private String paymentType;

	private PaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentType() {
		return paymentType;
	}
	
	

}
