package org.bookstore.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.bookstore.serialization.DateTimeSerializer;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name="Orders")
public class Order implements Serializable {

	private static final long serialVersionUID = -4648318537074302020L;
	
	private int id;
	private Integer orderNumber;
	private BigDecimal totalCost;
	private DateTime orderDate;
	private boolean paid;
	private Account account;
	private Set<OrderBook> orderBooks = new HashSet<OrderBook>();
	private PaymentOption paymentOption;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_seq_gen")
	@SequenceGenerator(name = "order_seq_gen", sequenceName = "order_sequence")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Min(0)
	@Column(nullable=false, precision=10, scale=2)
	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	@JsonSerialize(using = DateTimeSerializer.class)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(nullable=false)
	public DateTime getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(DateTime orderDate) {
		this.orderDate = orderDate;
	}
	
	@Column(nullable=false, unique=true)
	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	@Type(type = "true_false")
	@Column(nullable=false)
	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	@ManyToOne
	@JoinColumn(name="account_id", nullable=false)
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@JsonIgnore
	@OneToMany(mappedBy="order", cascade=CascadeType.REMOVE)
	public Set<OrderBook> getOrderBooks() {
		return orderBooks;
	}

	public void setOrderBooks(Set<OrderBook> orderBooks) {
		this.orderBooks = orderBooks;
	}
	
	@OneToOne(mappedBy="order", cascade=CascadeType.REMOVE)
	public PaymentOption getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(PaymentOption paymentOption) {
		this.paymentOption = paymentOption;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Order))
			return false;
		Order other = (Order) obj;
		if (getId() != other.getId())
			return false;
		if (getOrderNumber() == null) {
			if (other.getOrderNumber() != null)
				return false;
		}
		else if (!getOrderNumber().equals(other.getOrderNumber())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getId();
		result = prime * result + ((getOrderNumber() == null) ? 0 : getOrderNumber().hashCode());
		return result;
	}
	
	
	
	
	
	
}
