package org.bookstore.controller;

import java.util.List;
import java.util.Map;

import org.bookstore.Asserts;
import org.bookstore.model.Book;
import org.bookstore.model.WishList;
import org.bookstore.service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WishListController {

	@Autowired
	private WishListService wishListService;
	
	@RequestMapping(value="/addBookToList", method=RequestMethod.PUT)
	public WishList addBookToList(@RequestBody Map<String, String> wishMap) {
		String username = wishMap.get("username");
		Asserts.argumentIsNotNull(username, "Username");
		Integer bookId = Integer.parseInt(wishMap.get("bookId"));
		return wishListService.addBookToList(username, bookId);
	}
	
	@RequestMapping(value="/removeBookFromList", method=RequestMethod.DELETE)
	public WishList removeBookFromList(@RequestParam String username, @RequestParam Integer bookId) {
		Asserts.argumentIsNotNull(username, "Username"); 
		return wishListService.removeBookFromList(username, bookId);
	}
	
	@RequestMapping(value="/findWishList", method=RequestMethod.GET)
	public WishList findWishList(@RequestParam String username) {
		Asserts.argumentIsNotNull(username, "Username");
		return wishListService.findWishList(username);
	}

	@RequestMapping(value="/readWishList", method=RequestMethod.GET)
	public List<Book> readWishList(@RequestParam String username) {
		Asserts.argumentIsNotNull(username, "Username");
		return wishListService.readWishList(username);
	}
}
