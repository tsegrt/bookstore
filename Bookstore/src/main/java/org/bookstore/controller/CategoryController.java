package org.bookstore.controller;

import java.util.List;
import java.util.Map;

import org.bookstore.Asserts;
import org.bookstore.model.Category;
import org.bookstore.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(value="/addCategory", method=RequestMethod.POST)
	public Category addCategory(@RequestBody Map<String, String> categoryMap) {
		String name = categoryMap.get("name");
		Asserts.argumentIsNotNull(name, "Name");
		return categoryService.addCategory(name);
	}
	
	@RequestMapping(value="/listCategories", method=RequestMethod.GET)
	public List<Category> listCategories() {
		return categoryService.listAllCategories();
	}
	
	@RequestMapping(value="/findCategory", method=RequestMethod.GET)
	public Category findCategory(@RequestParam String name) {
		Asserts.argumentIsNotNull(name, "Name");
		return categoryService.findCategoryByName(name);
	}
	
	@RequestMapping(value="/findCategoryById", method=RequestMethod.GET)
	public Category findCategoryById(@RequestParam int categoryId) {
		return categoryService.findCategoryById(categoryId);
	}
	
	@RequestMapping(value="/renameCategory", method=RequestMethod.PUT)
	public Category renameCategory(@RequestBody Map<String, String> categoryMap) {
		String name = categoryMap.get("name");
		Asserts.argumentIsNotNull(name, "Name");
		String newName = categoryMap.get("newName");
		Asserts.argumentIsNotNull(newName, "New name");
		return categoryService.renameCategory(name, newName);
		
	}
	
	@RequestMapping(value="/removeCategory", method=RequestMethod.DELETE)
	public void removeCategory(@RequestParam String name) {
		Asserts.argumentIsNotNull(name, "Name");
		categoryService.deleteCategory(name);
	}
		
}
