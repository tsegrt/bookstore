package org.bookstore.controller;

import java.util.Map;

import org.bookstore.Asserts;
import org.bookstore.model.PaymentOption;
import org.bookstore.service.PaymentOptionService;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentOptionController {
	
	@Autowired
	private PaymentOptionService paymentOptionService;

	@RequestMapping(value="/addCard", method=RequestMethod.POST)
	public PaymentOption addCard(@RequestBody Map<String, String> cardMap) {
		String username = cardMap.get("username");
		Asserts.argumentIsNotNull(username, "Username");
		String fullName = cardMap.get("fullName");
		Asserts.argumentIsNotNull(fullName, "Full name");
		String cardNumber = cardMap.get("cardNumber");
		Asserts.argumentIsNotNull(cardNumber, "Card number");
		String CVV2 = cardMap.get("CVV2");
		Asserts.argumentIsNotNull(CVV2, "CVV2");
		String type = cardMap.get("type");
		Asserts.argumentIsNotNull(type, "Type");
		LocalDate expirationDate = LocalDate.parse(cardMap.get("expirationDate"));
		return paymentOptionService.addPaymentOption(username, fullName, cardNumber, CVV2, expirationDate, type);	
	}
	
	@RequestMapping(value="/findOption", method=RequestMethod.GET)
	public PaymentOption findOption(@RequestParam Integer orderNumber) {
		return paymentOptionService.findByOrder(orderNumber);
	}
	
}
