package org.bookstore.controller;

import java.util.Map;

import org.bookstore.Asserts;
import org.bookstore.model.ContactInfo;
import org.bookstore.service.ContactInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContactInfoController {
	
	@Autowired
	private ContactInfoService contactInfoService;
	
	@RequestMapping(value="/editContactInfo", method=RequestMethod.PUT) 
	public ContactInfo editContactInfo(@RequestBody Map<String, Object> contactMap) {
		Integer accountId = Integer.parseInt(contactMap.get("accountId").toString());
		String address = contactMap.get("address").toString();
		Asserts.argumentIsNotNull(address, "Address");
		String city = contactMap.get("city").toString();
		Asserts.argumentIsNotNull(city, "City");
		String postalCode = null;
		if(contactMap.containsKey("postalCode"))
			postalCode = contactMap.get("postalCode").toString();
		String country = contactMap.get("country").toString();
		Asserts.argumentIsNotNull(country, "Country");
		return contactInfoService.editContactInfo(accountId, address, city, postalCode, country);	
	}
	
	@RequestMapping(value="/findContactInfo", method=RequestMethod.GET) 
	public ContactInfo editContactInfo(@RequestParam Integer accountId) {
		return contactInfoService.findContactInfo(accountId);	
	}
}
