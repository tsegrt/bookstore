package org.bookstore.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.bookstore.Asserts;
import org.bookstore.model.Book;
import org.bookstore.service.BookService;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

	@Autowired
	private BookService bookService;
	
	@RequestMapping(value="/searchBooks", method=RequestMethod.GET)
	public List<Book> searchBooks(@RequestParam String details) {
		return bookService.findBooks(details);
	}
	
	@RequestMapping(value="/findBooksByCategory", method=RequestMethod.GET)
	public List<Book> findBooksByCategory(@RequestParam String categoryName) {
		return bookService.findBooksByCategory(categoryName);
	}

	@RequestMapping(value="/findBookById", method=RequestMethod.GET)
	public Book findBookById(@RequestParam int bookId) {
		return bookService.findBookById(bookId);
	}
	
	@RequestMapping(value="/findBooksByOrder", method=RequestMethod.GET)
	public List<Map<String,Object>> findBooksByOrder(@RequestParam String username) {
		return bookService.findBookByOrder(username);
	}
		
	@RequestMapping(value="/newBook", method=RequestMethod.POST)
	public Book newBook(@RequestBody Map<String, String> bookMap) {
		try {
			String title = bookMap.get("title");
			Asserts.argumentIsNotNull(title, "Title");
			String author = bookMap.get("author");
			Asserts.argumentIsNotNull(author, "Author");
			String language = bookMap.get("language");
			Asserts.argumentIsNotNull(language, "Language");
			String description = bookMap.get("description");
			Asserts.argumentIsNotNull(description, "Description");
			BigDecimal price = new BigDecimal(bookMap.get("price"));
			String publisher = bookMap.get("publisher");
			Asserts.argumentIsNotNull(publisher, "Publisher");
			LocalDate releaseDate = LocalDate.parse(bookMap.get("releaseDate"));
			Integer availableItems = Integer.parseInt(bookMap.get("availableItems"));
			String ISBN = bookMap.get("isbn");
			Asserts.argumentIsNotNull(ISBN, "ISBN");
			String cover = bookMap.get("cover");
			Integer categoryId = Integer.parseInt(bookMap.get("category"));
			return bookService.addBook(title, author, language, cover, description, publisher, releaseDate, ISBN, availableItems, price, categoryId);	
		}
		catch (DataIntegrityViolationException ex) {
			throw new IllegalArgumentException("Book with this ISBN number already exists!");
		}
	}
	
	@RequestMapping(value="/listBooks", method=RequestMethod.GET)
	public List<Book> listBooks() {
		return bookService.listAllBooks();
	}
	
	@RequestMapping(value="/findNewestBooks", method=RequestMethod.GET)
	public List<Book> findNewestBooks() {
		return bookService.findLatestReleases();
	}
	
	@RequestMapping(value="/changeBookPrice", method=RequestMethod.PUT)
	public Book changeBookPrice(@RequestBody Map<String, Object> bookMap) {
		Integer bookId = Integer.parseInt(bookMap.get("id").toString());
		BigDecimal price = new BigDecimal(bookMap.get("price").toString());
		return bookService.changeBookPrice(bookId, price);
	}
	
	@RequestMapping(value="/changeBookDescription", method=RequestMethod.PUT)
	public Book changeBookDescription(@RequestBody Map<String, Object> bookMap) {
		Integer bookId = Integer.parseInt(bookMap.get("id").toString());
		String cover = (String) bookMap.get("cover");
		String description = (String) bookMap.get("description");
		Asserts.argumentIsNotNull(description, "Description");
		return bookService.changeBookDescription(bookId, cover, description);
	}
	
	@RequestMapping(value="/changeBookAvailability", method=RequestMethod.PUT)
	public Book changeBookAvailability(@RequestBody Map<String, Object> bookMap) {
		Integer bookId =  Integer.parseInt(bookMap.get("id").toString());
		Integer availableItems = Integer.parseInt(bookMap.get("availableItems").toString());
		return bookService.changeBookAvailability(bookId, availableItems);
	}
	
	@RequestMapping(value="/removeBook", method=RequestMethod.DELETE)
	public void removeBook(@RequestParam Integer bookId) {
		bookService.deleteBook(bookId);
	}
	
	@RequestMapping(value="/changeBookCategory", method=RequestMethod.PUT)
	public Book changeBookCategory(@RequestBody Map<String, Object> bookMap) {
		Integer bookId = Integer.parseInt(bookMap.get("id").toString());
		String categoryName = bookMap.get("categoryName").toString();
		Asserts.argumentIsNotNull(categoryName, "Category");
		return bookService.changeBookCategory(bookId, categoryName);
	}
	
}
