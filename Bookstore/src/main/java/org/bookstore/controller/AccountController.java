package org.bookstore.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bookstore.Asserts;
import org.bookstore.model.Account;
import org.bookstore.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;
	
	@RequestMapping(value="/newUser", method=RequestMethod.POST)
	public Account newUser(@RequestBody Map<String, String> accountMap) {
		try {
			String firstName = accountMap.get("firstName");
			Asserts.argumentIsNotNull(firstName, "First name");
			String lastName = accountMap.get("lastName");
			Asserts.argumentIsNotNull(lastName, "Last name");
			String email = accountMap.get("email");
			Asserts.argumentIsNotNull(email, "Email"); 
			String username = accountMap.get("username");
			Asserts.argumentIsNotNull(username, "Username");
			String password = accountMap.get("password");
			Asserts.argumentIsNotNull(password, "Password");
			String type = accountMap.get("type");
			return accountService.registerUser(firstName, lastName, email, username, password, type);
		}
		catch (DataIntegrityViolationException ex) {
			throw new IllegalArgumentException("Username or email already exists!");
		}
	}
	
	@RequestMapping(value="/findAccountByUsername", method=RequestMethod.GET)
	public Account findAccountByUsername(@RequestParam String username) {
		Asserts.argumentIsNotNull(username, "Username");
		return accountService.findByUsername(username);
	}
	
	@RequestMapping(value="/findPrincipal", method=RequestMethod.GET)
	public Map<String,String> findPrincipal() {
		Map<String, String> accountMap = new HashMap<String, String>();
		accountMap.put("username", getPrincipal());
		return accountMap;
	}
	
	@RequestMapping(value="/updateEmail", method=RequestMethod.PUT)
	public Account updateEmail(@RequestBody Map<String, String> accountMap) {
		try {
			String email = accountMap.get("email");
			Asserts.argumentIsNotNull(email, "Email");
			String username = accountMap.get("username");
			Asserts.argumentIsNotNull(username, "Username");
			String password = accountMap.get("password");
			Asserts.argumentIsNotNull(password, "Password");
			return accountService.changeEmail(email, username, password);
		}
		catch (DataIntegrityViolationException ex) {
			throw new IllegalArgumentException("Email already exists!");
		}
	}
	
	@RequestMapping(value="/updatePassword", method=RequestMethod.PUT) 
	public Account updatePassword(@RequestBody Map<String, String> accountMap) {
		String username = accountMap.get("username");
		Asserts.argumentIsNotNull(username, "Username");
		String oldPassword = accountMap.get("oldPassword");
		Asserts.argumentIsNotNull(oldPassword, "Current password");
		String newPassword = accountMap.get("newPassword");
		Asserts.argumentIsNotNull(newPassword, "New password");
		String repeatedPassword = accountMap.get("repeatedPassword");
		Asserts.argumentIsNotNull(repeatedPassword, "New passsword");
		return accountService.changePassword(username, oldPassword, newPassword, repeatedPassword);
	}
	
	@RequestMapping(value="/findAccountById", method=RequestMethod.GET)
	public Account findAccountById(@RequestParam int id){
		return accountService.findById(id);
	}
	
	
	@RequestMapping(value="/listAccounts", method=RequestMethod.GET)
	public List<Account> listAccounts(@RequestParam String type){
		return accountService.listAccounts(type);
	}

	
	@RequestMapping(value="/updateState", method=RequestMethod.PUT)
	public Account updateState(@RequestParam String username) {
		Asserts.argumentIsNotNull(username, "Username");
		return accountService.changeState(username);
	}

	@RequestMapping(value="/deleteAccount", method=RequestMethod.DELETE)
	public void deleteAccount(@RequestParam String username) {
		Asserts.argumentIsNotNull(username, "Username");
		accountService.deleteAccount(username);
	}
	
	
	private String getPrincipal(){
		String username = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		else {
			username = principal.toString();
		}
		
		return username;
	}
}
