package org.bookstore.controller;

import java.util.List;
import java.util.Map;

import org.bookstore.Asserts;
import org.bookstore.model.Order;
import org.bookstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value="/addBookToOrder", method=RequestMethod.PUT)
	private Order addBookToOrder(@RequestBody Map<String, Object> orderMap) {
		String username = orderMap.get("username").toString();
		Asserts.argumentIsNotNull(username, "Username");
		Integer bookId = Integer.parseInt(orderMap.get("bookId").toString());
		return orderService.addBookToOrder(username, bookId);
	}
	
	@RequestMapping(value="/removeBookFromOrder", method=RequestMethod.PUT)
	private Order removeBookFromOrder(@RequestBody Map<String, String> orderMap) {
		String username = orderMap.get("username");
		Integer bookId = Integer.parseInt(orderMap.get("bookId"));
		return orderService.removeBookFromOrder(username, bookId);
	}
	
	@RequestMapping(value="/listOrders", method=RequestMethod.GET)
	public List<Order> listOrders() {
		return orderService.listOrders();
	}
	
	@RequestMapping(value="/findOrderByNumber", method=RequestMethod.GET)
	public Order findOrder(@RequestParam Integer orderNumber) {
		return orderService.findOrderByNumber(orderNumber);
	}
	
	@RequestMapping(value="/updateOrder", method=RequestMethod.PUT)
	public Order updateOrder(@RequestBody Map<String, String> orderMap) { 
		String username = orderMap.get("username");
		Integer bookId = Integer.parseInt(orderMap.get("bookId"));
		short quantity = Short.parseShort(orderMap.get("quantity"));
		return orderService.updateOrder(username, bookId, quantity);
	}
	
	@RequestMapping(value="/findOrdersByUser", method=RequestMethod.GET)
	public List<Order> findOrdersByUser(@RequestParam Integer accountId) {
		return orderService.findOrdersByUser(accountId);
	}
	
	@RequestMapping(value="/findActiveOrder", method=RequestMethod.GET)
	public Order findActiveOrder(@RequestParam String username) {
		Asserts.argumentIsNotNull(username, "Username");
		return orderService.findActiveOrder(username);
	}
	
}
