package org.bookstore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	
	@RequestMapping(value = "/admin/", method = RequestMethod.GET)
	public String adminPage(){
		return "index";
	}
	
	@RequestMapping(value = "/user/", method = RequestMethod.GET)
	public String userPage(){
		return "userHome";
	}
	
	@RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
	public String loginPage(){
		return "login";
	}

}
