package org.bookstore.security;

import java.util.ArrayList;
import java.util.List;

import org.bookstore.model.Account;
import org.bookstore.model.Role;
import org.bookstore.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookstoreUserDetailsService implements UserDetailsService {

	@Autowired
	private AccountService accountService;
	
	@Transactional(readOnly=true)
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = accountService.findByUsername(username);
		if(account == null){
			throw new UsernameNotFoundException("Username not found");
		}
		return new User(account.getUsername(), account.getPassword(), account.getState().equals("Active"), true, true, true, getGrantedAuthorities(account));
	}
	
	private List<GrantedAuthority> getGrantedAuthorities(Account account){
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		for(Role role : account.getRoles() ) {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getType()));
		}
		
		return authorities;
	}

}
