package org.bookstore.serialization;

import org.joda.time.LocalDate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.module.SimpleModule;

@Component
public class LocalDateModule extends SimpleModule{

	private static final long serialVersionUID = 876618458182762325L;
	
	public LocalDateModule(){
		super();
		addSerializer(LocalDate.class,new LocalDateSerializer());
	}

}
