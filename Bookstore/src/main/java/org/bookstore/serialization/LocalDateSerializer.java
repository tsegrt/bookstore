package org.bookstore.serialization;

import java.io.IOException;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class LocalDateSerializer extends StdSerializer<LocalDate> {

	private static final long serialVersionUID = -8287988633004772077L;

	protected LocalDateSerializer() {
		super(LocalDate.class);
	}


	@Override
	public void serialize(LocalDate date, JsonGenerator generator, SerializerProvider provider)
			throws IOException {
		String formattedDate = date.toString("d MMMM, yyyy");
		generator.writeString(formattedDate);
		
	}

}
