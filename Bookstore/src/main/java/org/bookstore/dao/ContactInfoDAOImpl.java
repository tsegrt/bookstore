package org.bookstore.dao;

import org.bookstore.model.Account;
import org.bookstore.model.ContactInfo;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ContactInfoDAOImpl implements ContactInfoDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private AccountDAO accountDAO;
	
	
	@Override
	public ContactInfo editContactInfo(Integer accountId, String address, String city, String postalCode, String country) {
		Account account = accountDAO.findById(accountId);
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ContactInfo.class)
															  .add(Restrictions.eq("account",account));
		ContactInfo contactInfo = (ContactInfo) criteria.uniqueResult();
		if (contactInfo == null) {
			contactInfo = new ContactInfo();
			contactInfo.setAccount(account);
		}
		contactInfo.setAddress(address);
		contactInfo.setPostalCode(postalCode);
		contactInfo.setCity(city);
		contactInfo.setCountry(country);
		sessionFactory.getCurrentSession().saveOrUpdate(contactInfo);
		return contactInfo;
	}
	
	@Override
	public ContactInfo findContactInfo(Integer accountId) {
		Account account = accountDAO.findById(accountId);
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ContactInfo.class)
															  .add(Restrictions.eq("account", account));
		ContactInfo contactInfo = (ContactInfo) criteria.uniqueResult();
		return contactInfo;
	}

}
