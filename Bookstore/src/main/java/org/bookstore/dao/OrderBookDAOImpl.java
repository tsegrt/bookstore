package org.bookstore.dao;

import java.util.List;

import org.bookstore.model.Book;
import org.bookstore.model.Order;
import org.bookstore.model.OrderBook;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderBookDAOImpl implements OrderBookDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Override
	public OrderBook addBookToCart(Order order, Book book) {
		OrderBook orderBook = findOrderBook(order, book);
		if (book.isAvailable()) {
			if (orderBook == null) {
				orderBook = new OrderBook();
				orderBook.setOrder(order);
				orderBook.setBook(book);
			}
			orderBook.setQuantity((short) (orderBook.getQuantity() + 1));
			book.setAvailableItems(book.getAvailableItems() - 1);
			if (book.getAvailableItems() == 0) 
				book.setAvailable(false);
		}
		sessionFactory.getCurrentSession().saveOrUpdate(book);
		sessionFactory.getCurrentSession().saveOrUpdate(orderBook);
		return orderBook;
	}

	@Override
	public void removeBookFromCart(Order order, Book book) {
		OrderBook orderBook = findOrderBook(order,book);
		book.setAvailableItems(book.getAvailableItems()+orderBook.getQuantity());
		if (book.getAvailableItems() > 0) {
			book.setAvailable(true);
		}
		sessionFactory.getCurrentSession().saveOrUpdate(book);
		sessionFactory.getCurrentSession().delete(orderBook);
	}

	@Override
	public short updateQuantity(Order order, Book book, short quantity) {
		OrderBook orderBook = findOrderBook(order,book);
		short difference = 0;
		if (orderBook != null) {
	        difference = (short) (quantity - orderBook.getQuantity());
	        if( book.getAvailableItems() >= difference) {
				book.setAvailableItems(book.getAvailableItems() - difference);
				if (book.getAvailableItems() > 0) {
					book.setAvailable(true);
				}
				else {
					book.setAvailable(false);
				}
				orderBook.setQuantity(quantity);
				sessionFactory.getCurrentSession().saveOrUpdate(book);
				sessionFactory.getCurrentSession().saveOrUpdate(orderBook);
	        }
	        else {
	        	difference = 0;
	        }
		}
		return difference;
	}

	@Override
	public OrderBook findOrderBook(Order order, Book book) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(OrderBook.class)
				  .add(Restrictions.and(
						  Restrictions.eq("order", order),
						  Restrictions.eq("book", book)));
		OrderBook orderBook = (OrderBook) criteria.uniqueResult();
		if (orderBook != null) {
			return orderBook;
		}
		else {
			return null;
		}
	}

	@Override
	public List<OrderBook> findOrderBooks(Order order) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(OrderBook.class)
															  .add(Restrictions.eq("order", order));
		@SuppressWarnings("unchecked")
		List<OrderBook> orderBooks = (List<OrderBook>) criteria.list();
		return orderBooks;
	}
		

}
