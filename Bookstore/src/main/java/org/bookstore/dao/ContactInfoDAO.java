package org.bookstore.dao;

import org.bookstore.model.ContactInfo;

public interface ContactInfoDAO {
	
	public ContactInfo findContactInfo(Integer accountId);
	
	public ContactInfo editContactInfo(Integer accountId, String address, String city, String postalCode, String country);

}
