package org.bookstore.dao;

import java.util.List;

import org.bookstore.model.Category;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryDAOImpl implements CategoryDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Category addCategory(String name) {
		Category category = new Category();
		category.setName(name);
		sessionFactory.getCurrentSession().saveOrUpdate(category);
		return category;
	}

	@Override
	public Category findCategoryByName(String name) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Category.class)
				   											  .add(Restrictions.eq("name", name));
		Category category = (Category) criteria.uniqueResult();
		return category;
	}

	@Override
	public List<Category> listAllCategories() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Category.class);
		@SuppressWarnings("unchecked")
		List<Category> categoryList = (List<Category>) criteria.list();
		return categoryList;
	}

	@Override
	public Category renameCategory(String oldName, String newName) {
		Category category = findCategoryByName(oldName);
		if (findCategoryByName(newName) == null) {
			category.setName(newName);
			sessionFactory.getCurrentSession().saveOrUpdate(category);
			return category;
		}
		else {
			return null;
		}
	}

	@Override
	public void deleteCategory(String name) {
		Category category = findCategoryByName(name);
		if (category != null) {
			sessionFactory.getCurrentSession().delete(category);
		}
	}

	@Override
	public Category findCategoryById(int categoryId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Category.class)
															   .add(Restrictions.eq("id", categoryId));
		Category category = (Category) criteria.uniqueResult();
		if (category != null) {
			return category;
		}
		else {
			return null;
		}
	}

}
