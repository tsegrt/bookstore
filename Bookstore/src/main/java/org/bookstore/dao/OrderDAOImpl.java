package org.bookstore.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bookstore.model.Account;
import org.bookstore.model.Book;
import org.bookstore.model.Order;
import org.bookstore.model.OrderBook;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderDAOImpl implements OrderDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private AccountDAO accountDAO;
	
	@Autowired
	private BookDAO bookDAO;
	
	@Autowired
	private OrderBookDAO orderBookDAO;


	public Order createOrder(Account account, Book book) {
		Order order = new Order();
		order.setAccount(account);
		int generatedNumber = generateOrderNumber();
		while (findOrderByNumber(generatedNumber) != null) {
			generatedNumber = generateOrderNumber();
		}
		order.setOrderNumber(generatedNumber);
		order.setPaid(false);
		order.setTotalCost(new BigDecimal(0));
		order.setOrderDate(DateTime.now(DateTimeZone.UTC));
		return order;
	}
	
	@Override
	public Order findOrderByNumber(Integer orderNumber) {
		String hql = "from Order where orderNumber = " + orderNumber;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		Order order = (Order) query.uniqueResult();
		if (order != null){
			return order;
		}
		else {
			return null;
		}
	}
	
	public int generateOrderNumber() {
		return (int)(Math.random()*(999999999-100000000)+100000000);
	}

	@Override
	public Order findActiveOrder(String username) {
		Account account = accountDAO.findByUsername(username);
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Order.class)
															  .add(Restrictions.and(
																	  Restrictions.eq("account", account),
																	  Restrictions.eq("paid", false)));
		Order order = (Order) criteria.uniqueResult();
		if (order != null) {
			return order;
		}
		else {
			return null;
		}
	}
	
	@Override
	public Order addBookToOrder(String username, Integer bookId) {
		Account account = accountDAO.findByUsername(username);
		Book book = bookDAO.findBookById(bookId);
		Order order = findActiveOrder(username);
		if (account != null && book != null && book.isAvailable()) {
			if (order == null) {
				order = createOrder(account, book);
			}
			sessionFactory.getCurrentSession().saveOrUpdate(order);
			OrderBook orderBook = orderBookDAO.addBookToCart(order, book);
			order.setTotalCost(order.getTotalCost().add(book.getPrice()));
			Set<OrderBook> orderBooks = order.getOrderBooks();
			orderBooks.add(orderBook);
			order.setOrderBooks(orderBooks);
			order.setOrderDate(DateTime.now(DateTimeZone.UTC));
			sessionFactory.getCurrentSession().saveOrUpdate(order);
		}
		return order;
	}
	
	@Override
	public Order removeBookFromOrder(String username, Integer bookId) {
		Account account = accountDAO.findByUsername(username);
		Book book = bookDAO.findBookById(bookId);
		if (account != null && book != null) {
			Order order = findActiveOrder(username);
			OrderBook orderBook = orderBookDAO.findOrderBook(order, book);
			if (orderBook != null) {
				order.setTotalCost(order.getTotalCost().subtract(book.getPrice().multiply(new BigDecimal(orderBook.getQuantity()))));
				orderBookDAO.removeBookFromCart(order, book);
				Set<OrderBook> orderBooks = order.getOrderBooks();
				orderBooks.remove(orderBook);
				order.setOrderBooks(orderBooks);
				order.setOrderDate(DateTime.now(DateTimeZone.UTC));
				sessionFactory.getCurrentSession().saveOrUpdate(order);
			}
			return order;
		}
		else {
			return null;
		}
	}
	

	@Override
	public List<Order> listOrders() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Order.class)
															  .add(Restrictions.eq("paid", true));
		@SuppressWarnings("unchecked")
		List<Order> orderList = (List<Order>) criteria.list();
		return orderList;
	}

	@Override
	public Order updateOrder(String username, Integer bookId, short quantity) {
		Account account = accountDAO.findByUsername(username);
		Book book = bookDAO.findBookById(bookId);
		if (account != null && book != null) {
			Order order = findActiveOrder(username);
			short difference = orderBookDAO.updateQuantity(order, book, quantity);
			order.setTotalCost(order.getTotalCost().add(book.getPrice().multiply(new BigDecimal(difference))));
			order.setOrderDate(DateTime.now(DateTimeZone.UTC));
			sessionFactory.getCurrentSession().saveOrUpdate(order);
			return order;
		}
		else {
			return null;
		}
	}

	@Override
	public List<Order> findOrdersByUser(Integer accountId) {
		Account account = accountDAO.findById(accountId);
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Order.class)
															  .add(Restrictions.and(
																	  Restrictions.eq("account", account),
																	  Restrictions.eq("paid",true)));
		@SuppressWarnings("unchecked")
		List<Order> orderList = (List<Order>) criteria.list();
		return orderList;
	}

	@Override
	public List<Map<String, Object>> findBooksByOrder(String username) {
		Order order = findActiveOrder(username);
		List<Map<String, Object>> bookList = new ArrayList<Map<String, Object>>();
		if (order != null) {
			List<OrderBook> orderBooks = orderBookDAO.findOrderBooks(order);
			for(OrderBook orderBook : orderBooks) {
				Map<String, Object> bookMap = new HashMap<String, Object>();
				bookMap.put("quantity", orderBook.getQuantity());
				bookMap.put("book", orderBook.getBook());
				bookList.add(bookMap);
			}
		}
		return bookList;
	}

}



