package org.bookstore.dao;

import java.util.List;

import org.bookstore.model.Book;
import org.bookstore.model.WishList;
import org.bookstore.model.WishListBook;

public interface WishListBookDAO {
	
	public WishListBook addBook(WishList wishList, Book book);
	
	public List<Book> readBooks(WishList wishList);

	public void removeWishListBook(Book book);

}
