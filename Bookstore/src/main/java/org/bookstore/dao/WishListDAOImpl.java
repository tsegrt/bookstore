package org.bookstore.dao;


import java.util.List;
import org.bookstore.model.Account;
import org.bookstore.model.Book;
import org.bookstore.model.WishList;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WishListDAOImpl implements WishListDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private AccountDAO accountDAO;
	
	@Autowired
	private BookDAO bookDAO;
	
	@Autowired
	private WishListBookDAO wishListBookDAO;
	
	@Override
	public WishList addBookToList(String username, Integer bookId) {
		Book book = bookDAO.findBookById(bookId);
		WishList wishList = findWishList(username);
		wishListBookDAO.addBook(wishList, book);
		sessionFactory.getCurrentSession().saveOrUpdate(wishList);
		return wishList;
	}

	@Override
	public WishList removeBookFromList(String username, Integer bookId) {
		Book book = bookDAO.findBookById(bookId);
		WishList wishList = findWishList(username);
		List<Book> books = wishListBookDAO.readBooks(wishList);
		books.remove(book);
		sessionFactory.getCurrentSession().saveOrUpdate(wishList);
		return wishList;
	}

	@Override
	public WishList findWishList(String username) {
		Account account = accountDAO.findByUsername(username);
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(WishList.class)
															  .add(Restrictions.eq("account", account));
		WishList wishList = (WishList) criteria.uniqueResult();
		if (wishList == null) {
			wishList = new WishList();
			wishList.setAccount(account);
			sessionFactory.getCurrentSession().saveOrUpdate(wishList);;
		}
		return wishList;
	}

	@Override
	public List<Book> readWishList(String username) {
		WishList wishList = findWishList(username);
		List<Book> books = wishListBookDAO.readBooks(wishList);
		return books;
	}


}
