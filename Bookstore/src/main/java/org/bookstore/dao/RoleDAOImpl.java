package org.bookstore.dao;

import java.util.List;

import org.bookstore.model.Role;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RoleDAOImpl implements RoleDAO {
	
	@Autowired
	private SessionFactory sessionFactory;


	@Override
	public List<Role> findAll() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Role.class);
		@SuppressWarnings("unchecked")
		List<Role> roleList = (List<Role>) criteria.list();
		return roleList;
	}

	@Override
	public Role findByType(String type) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Role.class)
															  .add(Restrictions.eq("type", type));
		Role role = (Role) criteria.uniqueResult();
		if (role != null) {
			return role;
		}
		else {
			return null;
		}
	}

}
