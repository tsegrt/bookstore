package org.bookstore.dao;

import java.math.BigDecimal;
import java.util.List;
import org.bookstore.model.Book;
import org.bookstore.model.Category;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BookDAOImpl implements BookDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CategoryDAO categoryDAO;

	@Autowired
	private WishListBookDAO wishListBookDAO;
	
	
	@Override
	public List<Book> findBooks(String details) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Book.class)
															  .add(Restrictions.or(
																	  Restrictions.ilike("title", details, MatchMode.ANYWHERE),
																	  Restrictions.ilike("author", details, MatchMode.ANYWHERE),
																	  Restrictions.eq("ISBN", details)));
		@SuppressWarnings("unchecked")
		List<Book> bookList = (List<Book>) criteria.list();
		return bookList;
	}
	
	@Override
	public List<Book> findBooksByCategory(String categoryName) {
		Category category = categoryDAO.findCategoryByName(categoryName);
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Book.class)
															  .add(Restrictions.eq("category", category));
		@SuppressWarnings("unchecked")
		List<Book> bookList = (List<Book>) criteria.list();
		return bookList;
	}
	
	@Override
	public Book findBookById(int bookId) {
		String hql = "from Book where id = " + bookId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		Book book = (Book) query.uniqueResult();
		return book;
	}
	
	@Override
	public Book addBook(String title, String author, String language, String cover, String description, String publisher,
						LocalDate releaseDate, String ISBN, int availableItems, BigDecimal price, int categoryId) {
		Category category = categoryDAO.findCategoryById(categoryId);
		if (category != null) {
			if(releaseDate.isBefore(LocalDate.now()) || releaseDate.isEqual(LocalDate.now())) { 
				Book book = new Book();
				book.setTitle(title);
				book.setAuthor(author);
				book.setLanguage(language);
				book.setCover(cover);
				book.setDescription(description);
				book.setAvailableItems(availableItems);
				if (availableItems > 0) {
					book.setAvailable(true);
				}
				else {
					book.setAvailable(false);
				}
				book.setPublisher(publisher);
				book.setReleaseDate(releaseDate);
				book.setISBN(ISBN);
				book.setSoldItems(0);
				book.setPrice(price);
				book.setCategory(category);
				sessionFactory.getCurrentSession().saveOrUpdate(book);
				return book;
			}
			else	
				throw new IllegalArgumentException("Release date is not correct!");
		}
		else {
			return null;
		}
	}


	@Override
	public List<Book> listAllBooks() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Book.class);
		@SuppressWarnings("unchecked")
		List<Book> bookList = (List<Book>) criteria.list();
		return bookList;
	}
	
	@Override
	public Book changeBookPrice(int bookId, BigDecimal newPrice) {
		Book book = findBookById(bookId);
		if (book != null) {
			book.setPrice(newPrice);
			sessionFactory.getCurrentSession().saveOrUpdate(book);
			return book;
		}
		else {
			return null;
		}
	}

	@Override
	public Book changeBookAvailability(int bookId, int availableItems) {
		Book book = findBookById(bookId);
		if (book != null) {
			if (availableItems > 0) {
				book.setAvailableItems(availableItems);
				book.setAvailable(true);
			}
			else {
				book.setAvailableItems(0);
				book.setAvailable(false);
			}
			sessionFactory.getCurrentSession().saveOrUpdate(book);
			return book;
		}
		else {
			return null;
		}
	}

	@Override
	public Book changeBookDescription(int bookId, String cover, String description) {
		Book book = findBookById(bookId);
		if (book != null) {
			book.setCover(cover);
			book.setDescription(description);
			sessionFactory.getCurrentSession().saveOrUpdate(book);
			return book;
		}
		else {
			return null;
		}
	}

	@Override
	public void deleteBook(int bookId) {
		Book book = findBookById(bookId);
		if (book != null) {
			wishListBookDAO.removeWishListBook(book);
			sessionFactory.getCurrentSession().delete(book);
		}
	}

	@Override
	public Book changeBookCategory(int bookId, String categoryName) {
		Book book = findBookById(bookId);
		Category category = categoryDAO.findCategoryByName(categoryName);
		if (book != null && category != null) {
			book.setCategory(category);
			sessionFactory.getCurrentSession().saveOrUpdate(book);
			return book;
		}
		else {
			return null;
		}
	}

	@Override
	public List<Book> findLatestReleases() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Book.class)
															  .addOrder(Order.desc("releaseDate"))
															  .setMaxResults(9);
		@SuppressWarnings("unchecked")
		List<Book> bookList = (List<Book>) criteria.list();
		return bookList;
	}

}
