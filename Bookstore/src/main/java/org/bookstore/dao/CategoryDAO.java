package org.bookstore.dao;

import java.util.List;

import org.bookstore.model.Category;

public interface CategoryDAO {

	public Category addCategory(String name);
	
	public Category findCategoryByName(String name);
	
	public Category findCategoryById(int categoryId);
	
	public List<Category> listAllCategories();
	
	public Category renameCategory(String oldName, String newName);
	
	public void deleteCategory(String name);
}
