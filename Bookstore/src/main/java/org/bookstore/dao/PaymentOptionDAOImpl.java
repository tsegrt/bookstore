package org.bookstore.dao;

import java.util.Set;

import org.bookstore.model.Book;
import org.bookstore.model.Order;
import org.bookstore.model.OrderBook;
import org.bookstore.model.PaymentOption;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class PaymentOptionDAOImpl implements PaymentOptionDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private OrderDAO orderDAO;
	
	@Autowired
	private PasswordEncoder encoder;

	
	@Override
	public PaymentOption addPaymentOption(String username, String fullName, String cardNumber, String CVV2,
			LocalDate expirationDate, String type) {
		Order order = orderDAO.findActiveOrder(username);
		if (order != null) {
			if(expirationDate.isAfter(LocalDate.now(DateTimeZone.UTC))) {
				PaymentOption paymentOption = new PaymentOption();
				paymentOption.setFullName(fullName);
				paymentOption.setCardNumber(encoder.encode(cardNumber));
				paymentOption.setCVV2(encoder.encode(CVV2));
				paymentOption.setExpirationDate(expirationDate);
				paymentOption.setType(type);
				paymentOption.setOrder(order);
				order.setPaid(true);
				order.setOrderDate(DateTime.now(DateTimeZone.UTC));
				Set<OrderBook> orderBooks = order.getOrderBooks();
				for(OrderBook orderBook : orderBooks) {
					Book book = orderBook.getBook();
					book.setSoldItems(book.getSoldItems()+orderBook.getQuantity());
					sessionFactory.getCurrentSession().saveOrUpdate(book);
				}
				sessionFactory.getCurrentSession().saveOrUpdate(order);
				sessionFactory.getCurrentSession().saveOrUpdate(paymentOption);
				return paymentOption;
			}
			else	
				throw new IllegalArgumentException("Card has been expired!");
		
		}
		else {
			return null;
		}
	}
	
	@Override
	public PaymentOption findByOrder(Integer orderNumber) {
		Order order = orderDAO.findOrderByNumber(orderNumber);
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PaymentOption.class)
															  .add(Restrictions.eq("order", order));
		PaymentOption option = (PaymentOption) criteria.uniqueResult();
		if (option != null) {
			return option;
		}
		else {
			return null;
		}
	}

}
