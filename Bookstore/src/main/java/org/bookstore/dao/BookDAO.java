package org.bookstore.dao;

import java.math.BigDecimal;
import java.util.List;

import org.bookstore.model.Book;
import org.joda.time.LocalDate;

public interface BookDAO {
	
	public List<Book> findBooks(String details);
	
	public List<Book> findBooksByCategory(String categoryName);

	public Book findBookById(int bookId);
	
	public Book addBook(String title, String author, String language, String cover, String description, String publisher, LocalDate releaseDate, String ISBN, int availableItems, BigDecimal price, int categoryId);
	
	public List<Book> listAllBooks();
	
	public List<Book> findLatestReleases();

	public Book changeBookCategory(int bookId, String categoryName);
	
	public Book changeBookPrice(int bookId, BigDecimal newPrice);
	
	public Book changeBookAvailability(int bookId, int availableItems);
	
	public Book changeBookDescription(int bookId, String cover, String description);
	
	public void deleteBook(int bookId);

}
