package org.bookstore.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bookstore.model.Account;
import org.bookstore.model.Role;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class AccountDAOImpl implements AccountDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private RoleDAO roleDAO;
	
	@Autowired
	private PasswordEncoder encoder;
	
	@Override
	public Account registerUser(String firstName, String lastName, String email, String username, String password, String type) {
		Account account = new Account();
		account.setFirstName(firstName);
		account.setLastName(lastName);
		account.setEmail(email);
		account.setUsername(username);
		account.setPassword(encoder.encode(password));
		account.setRegistrationDate(LocalDate.now(DateTimeZone.UTC));
		account.setState("Active");
		Set<Role> roles = new HashSet<Role>();
		roles.add(roleDAO.findByType(type));
		account.setRoles(roles);
		sessionFactory.getCurrentSession().saveOrUpdate(account);
		return account;
	}
	
	@Override
	public Account findByUsername(String username) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Account.class)
															  .add(Restrictions.eq("username", username));
		Account account = (Account) criteria.uniqueResult();
		return account;
	}
	
	@Override
	public Account changeEmail(String email, String username, String password) {
		Account account = findByUsername(username);
		if (encoder.matches(password, account.getPassword())) {
			account.setEmail(email);
			sessionFactory.getCurrentSession().saveOrUpdate(account);
		}
		else
			throw new IllegalArgumentException("Invalid password!");
		return account;
	}
	
	@Override
	public Account changePassword(String username, String oldPassword, String newPassword, String repeatedPassword) {
		Account account = findByUsername(username);
		if (encoder.matches(oldPassword, account.getPassword())) {
			if (newPassword.equals(repeatedPassword)) {		
				account.setPassword(encoder.encode(newPassword));
				sessionFactory.getCurrentSession().saveOrUpdate(account);	
			}
			else 
				throw new IllegalArgumentException("Passwords don't match!");
		}
		else
			throw new IllegalArgumentException("Invalid current password!");
		return account;
	}

	@Override
	public Account findById(int id) {
		String hql = "from Account where id = " + id;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		Account account = (Account) query.uniqueResult();
		return account;
	}

	@Override
	public void save(Account account) {
		account.setPassword(encoder.encode(account.getPassword()));
		sessionFactory.getCurrentSession().saveOrUpdate(account);
	}

	@Override
	public List<Account> listAccounts(String type) {
		Role role = roleDAO.findByType(type);
		Set<Account> accounts = (Set<Account>) role.getAccounts();
		List<Account> accountList = new ArrayList<Account>(accounts);
		return accountList;
	}
	
	@Override
	public Account changeState(String username) {
		Account account = findByUsername(username);
		if (account != null) {
			if (account.getState().equals("Active")) {
				account.setState("Inactive");
			}
			else {
				account.setState("Active");
			}
			sessionFactory.getCurrentSession().saveOrUpdate(account);
			return account;
		}
		else {
			return null;
		}
		
	}


	@Override
	public void deleteAccount(String username) {
		Account account = findByUsername(username);
		if (account != null) {
			sessionFactory.getCurrentSession().delete(account);
		}
	}

}
