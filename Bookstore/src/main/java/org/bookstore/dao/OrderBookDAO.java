package org.bookstore.dao;

import java.util.List;

import org.bookstore.model.Book;
import org.bookstore.model.Order;
import org.bookstore.model.OrderBook;

public interface OrderBookDAO {
	
	public OrderBook addBookToCart(Order order, Book book);
	
	public void removeBookFromCart(Order order, Book book);
	
	public short updateQuantity(Order order, Book book, short quantity);
	
	public OrderBook findOrderBook(Order order, Book book);
	
	public List<OrderBook> findOrderBooks(Order order);
	
}
