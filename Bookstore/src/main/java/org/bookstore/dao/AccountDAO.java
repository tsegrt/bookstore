package org.bookstore.dao;

import java.util.List;

import org.bookstore.model.Account;

public interface AccountDAO {
	
	public Account registerUser(String firstName, String lastName, String email, String username, String password, String type);
	
	public Account findByUsername(String username);
	
	public Account changeEmail(String email, String username, String password);

	public Account changePassword(String username, String oldPassword, String newPassword, String repeatedPassword);
	
	public Account findById(int id);
	
	public void save(Account account);
	
	public List<Account> listAccounts(String type);
	
	public Account changeState(String username);
	
	public void deleteAccount(String username);
	
}
