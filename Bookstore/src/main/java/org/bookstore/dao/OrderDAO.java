package org.bookstore.dao;

import java.util.List;
import java.util.Map;

import org.bookstore.model.Order;

public interface OrderDAO {
	
	public Order findOrderByNumber(Integer orderNumber);
	
	public Order addBookToOrder(String username, Integer bookId);

	public Order removeBookFromOrder(String username, Integer bookId);
	
	public List<Order> listOrders();
	
	public Order updateOrder(String username, Integer bookId, short quantity);
	
	public Order findActiveOrder(String username);
	
	public List<Order> findOrdersByUser(Integer accountId);
	
	public List<Map<String, Object>> findBooksByOrder(String username);
	
	
}
