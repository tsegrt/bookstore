package org.bookstore.dao;

import java.util.List;

import org.bookstore.model.Role;

public interface RoleDAO {

	public List<Role> findAll();
	
	public Role findByType(String type);
}
