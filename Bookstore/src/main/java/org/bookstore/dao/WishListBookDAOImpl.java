package org.bookstore.dao;

import java.util.ArrayList;
import java.util.List;
import org.bookstore.model.Book;
import org.bookstore.model.WishList;
import org.bookstore.model.WishListBook;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WishListBookDAOImpl implements WishListBookDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Book> readBooks(WishList wishList) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(WishListBook.class)
				  .add(Restrictions.eq("wishList", wishList));
		@SuppressWarnings("unchecked")
		List<WishListBook> wishListBooks = criteria.list();
		List<Book> bookList = new ArrayList<Book>();
		for (WishListBook wishListBook : wishListBooks) {
			bookList.add(wishListBook.getBook());
		}
		return bookList;

	}

	@Override
	public void removeWishListBook(Book book) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(WishListBook.class)
				  .add(Restrictions.eq("book", book));
		@SuppressWarnings("unchecked")
		List<WishListBook> wishListBooks = criteria.list();
		for(WishListBook wishListBook : wishListBooks) {
			sessionFactory.getCurrentSession().delete(wishListBook);
		}
		
	}

	@Override
	public WishListBook addBook(WishList wishList, Book book) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(WishListBook.class)
				  .add(Restrictions.and(
						  Restrictions.eq("wishList", wishList),
						  Restrictions.eq("book", book)));
		WishListBook wishListBook = (WishListBook) criteria.uniqueResult();
		if (wishListBook == null) {
			wishListBook = new WishListBook();
			wishListBook.setWishList(wishList);
			wishListBook.setBook(book);
			sessionFactory.getCurrentSession().saveOrUpdate(wishListBook);
		}
		return wishListBook;
	
	}
	
	

}

