package org.bookstore.service;
import java.util.List;

import org.bookstore.dao.WishListDAO;
import org.bookstore.model.Book;
import org.bookstore.model.WishList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class WishListServiceImpl implements WishListService {

	@Autowired
	private WishListDAO wishListDAO;
	
	@Override
	public WishList addBookToList(String username, Integer bookId) {
		return wishListDAO.addBookToList(username, bookId);
		
	}

	@Override
	public WishList removeBookFromList(String username, Integer bookId) {
		return wishListDAO.removeBookFromList(username, bookId);
	}

	@Override
	public WishList findWishList(String username) {
		return wishListDAO.findWishList(username);
	}

	@Override
	public List<Book> readWishList(String username) {
		return wishListDAO.readWishList(username);
	}

}
