package org.bookstore.service;

import org.bookstore.dao.ContactInfoDAO;
import org.bookstore.model.ContactInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ContactInfoServiceImpl implements ContactInfoService {

	@Autowired
	private ContactInfoDAO contactInfoDAO;
	
	@Override
	public ContactInfo editContactInfo(Integer accountId, String address, String city, String postalCode, String country) {
		return contactInfoDAO.editContactInfo(accountId, address, city, postalCode, country);
	}

	@Override
	public ContactInfo findContactInfo(Integer accountId) {
		return contactInfoDAO.findContactInfo(accountId);
	}

}
