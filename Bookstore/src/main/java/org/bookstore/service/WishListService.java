package org.bookstore.service;

import java.util.List;

import org.bookstore.model.Book;
import org.bookstore.model.WishList;

public interface WishListService {
	
	public WishList addBookToList(String username, Integer bookId);
	
	public WishList removeBookFromList(String username, Integer bookId);
	
	public WishList findWishList(String username);

	public List<Book> readWishList(String username);

}
