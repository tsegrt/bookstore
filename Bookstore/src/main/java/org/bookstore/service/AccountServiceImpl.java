package org.bookstore.service;

import java.util.List;

import org.bookstore.dao.AccountDAO;
import org.bookstore.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class AccountServiceImpl implements AccountService {
	
	@Autowired
	private AccountDAO accountDAO;

	@Override
	public Account registerUser(String firstName, String lastName, String email, String username, String password, String type) {
		return accountDAO.registerUser(firstName, lastName, email, username, password, type);
	}
	
	@Override
	public Account findByUsername(String username) {
		return accountDAO.findByUsername(username);
	}
	
	@Override
	public Account changeEmail(String email, String username, String password) {
		return accountDAO.changeEmail(email, username, password);
	}
	
	@Override
	public Account changePassword(String username, String oldPassword, String newPassword, String repeatedPassword) {
		return accountDAO.changePassword(username, oldPassword, newPassword, repeatedPassword);
	}
	
	@Override
	public Account findById(int id) {
		return accountDAO.findById(id);
	}
	
	@Override
	public void save(Account account) {
		accountDAO.save(account);
	}

	@Override
	public List<Account> listAccounts(String type) {
		return accountDAO.listAccounts(type);
	}
	
	@Override
	public Account changeState(String username) {
		return accountDAO.changeState(username);
	}

	@Override
	public void deleteAccount(String username) {
		accountDAO.deleteAccount(username);
	}

}
