package org.bookstore.service;

import org.bookstore.model.ContactInfo;

public interface ContactInfoService {
	
	public ContactInfo findContactInfo(Integer accountId);
	
	public ContactInfo editContactInfo(Integer accountId, String address, String city, String postalCode, String country);
	
}
