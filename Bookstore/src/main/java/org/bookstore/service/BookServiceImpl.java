package org.bookstore.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.bookstore.dao.BookDAO;
import org.bookstore.dao.OrderDAO;
import org.bookstore.model.Book;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookDAO bookDAO;
	
	@Autowired
	private OrderDAO orderDAO;

	@Override
	public List<Book> findBooks(String details) {
		return bookDAO.findBooks(details);
	}
	
	@Override
	public List<Book> findBooksByCategory(String categoryName) {
		return bookDAO.findBooksByCategory(categoryName);
	}

	@Override
	public Book findBookById(int bookId) {
		return bookDAO.findBookById(bookId);
	}
	
	@Override
	public Book addBook(String title, String author, String language, String cover, String description,
			String publisher, LocalDate releaseDate, String ISBN, int availableItems, BigDecimal price, int categoryId) {
		return bookDAO.addBook(title, author, language, cover, description, publisher, releaseDate, ISBN, availableItems, price, categoryId);
	}

	@Override
	public List<Book> listAllBooks() {
		return bookDAO.listAllBooks();
	}

	@Override
	public Book changeBookPrice(int bookId, BigDecimal newPrice) {
		return bookDAO.changeBookPrice(bookId, newPrice);
	}

	@Override
	public Book changeBookAvailability(int bookId, int availableItems) {
		return bookDAO.changeBookAvailability(bookId, availableItems);
	}

	@Override
	public Book changeBookDescription(int bookId, String cover, String description) {
		return bookDAO.changeBookDescription(bookId, cover, description);
	}

	@Override
	public void deleteBook(int bookId) {
		bookDAO.deleteBook(bookId);
	}

	@Override
	public Book changeBookCategory(int bookId, String categoryName) {
		return bookDAO.changeBookCategory(bookId, categoryName);
	}

	@Override
	public List<Map<String,Object>> findBookByOrder(String username) {
		return orderDAO.findBooksByOrder(username);
	}

	@Override
	public List<Book> findLatestReleases() {
		return bookDAO.findLatestReleases();
	}

}
