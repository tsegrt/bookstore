package org.bookstore.service;

import java.util.List;

import org.bookstore.dao.OrderDAO;
import org.bookstore.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderDAO orderDAO;

	@Override
	public Order findOrderByNumber(Integer orderNumber) {
		return orderDAO.findOrderByNumber(orderNumber);
	}

	@Override
	public Order addBookToOrder(String username, Integer bookId) {
		return orderDAO.addBookToOrder(username, bookId);
	}

	@Override
	public Order removeBookFromOrder(String username, Integer bookId) {
		return orderDAO.removeBookFromOrder(username, bookId);
	}

	@Override
	public List<Order> listOrders() {
		return orderDAO.listOrders();
	}

	@Override
	public Order updateOrder(String username, Integer bookId, short quantity) {
		return orderDAO.updateOrder(username, bookId, quantity);
	}

	@Override
	public List<Order> findOrdersByUser(Integer accountId) {
		return orderDAO.findOrdersByUser(accountId);
	}

	@Override
	public Order findActiveOrder(String username) {
		return orderDAO.findActiveOrder(username);
	}

}
