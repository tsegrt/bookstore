package org.bookstore.service;

import org.bookstore.dao.PaymentOptionDAO;
import org.bookstore.model.PaymentOption;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class PaymentOptionServiceImpl implements PaymentOptionService{

	@Autowired
	private PaymentOptionDAO paymentOptionDAO;

	@Override
	public PaymentOption addPaymentOption(String username, String fullName, String cardNumber, String CVV2,
			LocalDate expirationDate, String type) {
		return paymentOptionDAO.addPaymentOption(username, fullName, cardNumber, CVV2, expirationDate, type);
	}
	
	@Override
	public PaymentOption findByOrder(Integer orderNumber) {
		return paymentOptionDAO.findByOrder(orderNumber);
	}

}
