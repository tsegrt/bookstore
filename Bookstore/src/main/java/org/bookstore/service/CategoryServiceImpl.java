package org.bookstore.service;

import java.util.List;

import org.bookstore.dao.CategoryDAO;
import org.bookstore.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryDAO categoryDAO;
	
	@Override
	public Category addCategory(String name) {
		return categoryDAO.addCategory(name);
	}

	@Override
	public Category findCategoryByName(String name) {
		return categoryDAO.findCategoryByName(name);
	}

	@Override
	public List<Category> listAllCategories() {
		return categoryDAO.listAllCategories();
	}

	@Override
	public Category renameCategory(String oldName, String newName) {
		return categoryDAO.renameCategory(oldName, newName);
	}

	@Override
	public void deleteCategory(String name) {
		categoryDAO.deleteCategory(name);
	}

	@Override
	public Category findCategoryById(int categoryId) {
		return categoryDAO.findCategoryById(categoryId);
	}

}
