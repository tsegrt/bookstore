package org.bookstore.service;

import java.util.List;

import org.bookstore.model.Role;

public interface RoleService {

	public List<Role> findAll();
	
	public Role findByType(String type);
	
}
