package org.bookstore.service;

import java.util.List;

import org.bookstore.dao.RoleDAO;
import org.bookstore.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDAO roleDAO;
	
	
	@Override
	@Transactional
	public List<Role> findAll() {
		return roleDAO.findAll();
	}

	@Override
	public Role findByType(String type) {
		return roleDAO.findByType(type);
	}

}
