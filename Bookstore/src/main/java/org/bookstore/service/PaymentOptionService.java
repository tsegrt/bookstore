package org.bookstore.service;

import org.bookstore.model.PaymentOption;
import org.joda.time.LocalDate;

public interface PaymentOptionService {

	public PaymentOption addPaymentOption(String username, String fullName, String cardNumber, String CVV2, LocalDate expirationDate, String type);
	
	public PaymentOption findByOrder(Integer orderNumber);

}
