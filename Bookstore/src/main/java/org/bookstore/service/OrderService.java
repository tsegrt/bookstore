package org.bookstore.service;

import java.util.List;
import org.bookstore.model.Order;

public interface OrderService {

	public Order findOrderByNumber(Integer orderNumber);
	
	public Order addBookToOrder(String username, Integer bookId);

	public Order removeBookFromOrder(String username, Integer bookId);
	
	public List<Order> listOrders();
	
	public Order updateOrder(String username, Integer bookId, short quantity);
	
	public List<Order> findOrdersByUser(Integer accountId);
	
	public Order findActiveOrder(String username);
	
}
