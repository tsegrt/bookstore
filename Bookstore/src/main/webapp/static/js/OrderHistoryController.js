(function(){
var home = angular.module("bookshop");

var OrderHistoryController = function($scope, $http, storeservice) {

	var onOrdersFound = function(response) {
		$scope.orders = response.data;
	}
	
	var onUserFound = function(data) {
		$scope.user = data;
		$http.get("/findOrdersByUser?accountId="+$scope.user.id).then(onOrdersFound);
	};

	var onPrincipalFound = function (data) {
		$scope.account = data;
		storeservice.getUser($scope.account.username).then(onUserFound)
	};
	
	storeservice.getPrincipal().then(onPrincipalFound);
	
	var onOrdersFound = function(response){
		$scope.orders = response.data;
	}

	
	
	$scope.searchBook = function(details) {
		$location.path("/books/results/"+details);
	}
		
};

home.controller("OrderHistoryController", OrderHistoryController);
}());