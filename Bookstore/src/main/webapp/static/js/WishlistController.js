(function(){
var home = angular.module("bookshop");

var WishlistController = function($scope, $http, $location, storeservice) {
	
	$scope.currentPage = 0;
	$scope.pageSize = 4;
	
	var onListReaded = function(response){
		$scope.books = response.data;
		if($scope.books.length > 0) {
			$scope.numberOfPages = Math.ceil($scope.books.length / $scope.pageSize);
		}
		else {
			$scope.numberOfPages = 1;
		}
		angular.forEach($scope.books, function(value,index){
			if (value.availableItems > 0)
				value.availability = "Available";
			else
				value.availability = "Not available";
		});
	};
	
	var onPrincipalFound = function(data) {
		$scope.account = data;
		$http.get("/readWishList?username=" + $scope.account.username).then(onListReaded);
	};
	
	storeservice.getPrincipal().then(onPrincipalFound);

	
	$scope.sortOrder = "-soldItems";
	
	$scope.searchBook = function(details) {
		$location.path("/books/results/" + details);
	};
	
	
	$scope.viewBook = function(bookId) {
		$location.path("/books/item/" + bookId);
	};
	
	
	var onBookRemoved = function(response, bookId) {
		$scope.wishlist = response.data
		var index = -1;		
		var array = eval ($scope.books);
		for (var i = 0; i < array.length; i++) {
			if(array[i].id === bookId) {
				index = i;
				break;
			}
		}
		$scope.books.splice(index, 1);
		if($scope.books.length > 0) {
			$scope.numberOfPages = Math.ceil($scope.books.length / $scope.pageSize);
		}
		else {
			$scope.numberOfPages = 1;
		}
		$scope.currentPage = 0;
		$scope.wishlist = {};
	};
	
	$scope.removeFromList = function(bookId, username) {
		$scope.wishlist = {username: username, bookId: bookId};
		$http["delete"]("/removeBookFromList?username=" + username + "&&bookId=" + bookId)
					   .then(function(response){onBookRemoved(response, bookId)});
	};

	
	var onBookOrdered = function(data) {
		$scope.order = data;
	};
	
	var onError = function(reason) {
		$scope.error = reason.data.message;
	};
	
	$scope.addToCart = function(bookId) {
		$scope.book = {username: $scope.account.username, bookId: bookId};
		storeservice.addBook($scope.book).then(onBookOrdered, onError);
	};

};

home.controller("WishlistController", WishlistController);
}());