(function(){
var home = angular.module("bookshop");

var BookItemController = function($scope, $http, $routeParams, $location, storeservice) {
	
	var onBookFound = function (response) {
		$scope.book = response.data;
		if ($scope.book.availableItems > 0) {
			$scope.book.availability = "Available";
			$scope.book.available = true;
		}
		else
			$scope.book.availability = "Not available";
		storeservice.getBooksByCategory($scope.book.category.name).then(onRelatedBooks);
	};
	
	$scope.bookId = $routeParams.bookId;
	$http.get("/findBookById?bookId=" + $scope.bookId).then(onBookFound);
	
	
	var onRelatedBooks = function(data) {
		$scope.books = data;
		var index = -1;		
		var array = eval ($scope.books);
		for (var i = 0; i < array.length; i++) {
			if(array[i].isbn === $scope.book.isbn) {
				index = i;
				break;
			}
		}
		$scope.books.splice(index, 1);	
		$scope.books.push($scope.book)
	};
	
	
	$scope.viewBook = function(bookId) {
		$location.path("/books/item/" + bookId);
	};

	
	$scope.searchBook = function(details) {
		$location.path("/books/results/" + details);
	};

	
	var onBookAdded = function(response) {
		$scope.wishlist = response.data;
	};
	
	var onError = function(reason) {
		$scope.error = reason.data.message;
	};
	
	var onPrincipalFound = function(data) {
		$scope.account = data;
	};
	
	storeservice.getPrincipal().then(onPrincipalFound);
	
	$scope.addToList = function(bookId, account) {
		$scope.wishlist = {bookId : bookId, username : account.username};
		$http.put("/addBookToList", $scope.wishlist).then(onBookAdded, onError);
	};
	

	var onBookOrdered = function(data) {
		$scope.order = data;
		$http.get("/findBookById?bookId=" + $scope.bookId).then(onBookFound);
	};
	
	$scope.addToCart = function(bookId) {
		$scope.book = {username: $scope.account.username, bookId: bookId};
		storeservice.addBook($scope.book).then(onBookOrdered, onError);
	};
	
};

home.controller("BookItemController", BookItemController);
}());