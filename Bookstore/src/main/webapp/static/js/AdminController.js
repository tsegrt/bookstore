(function(){
var app = angular.module("bookstore");

var AdminController = function($scope, $http, bookservice) {
	
		var onAdminComplete = function (response) {
			$scope.accounts = response.data;
		};
	
		$http.get("/listAccounts?type=ADMIN").then(onAdminComplete);
		
			
		var onRegister = function (response) {
			$scope.account = response.data	
			$scope.accounts.push($scope.account);
			$scope.account = {type:"ADMIN"};
			$scope.error = false;
			$("#myAdmin").modal("hide");
		};
		
		var onError = function(reason) {
			$scope.error = reason.data.message;
		};
		
		$scope.account = {type:"ADMIN"};
		
		$scope.register = function () {
			$http.post("/newUser", $scope.account).then(onRegister, onError);
		};
		
		
		var onChange = function(data){
			$scope.account = data
			var index = -1;		
			var array = eval($scope.accounts);
			for (var i = 0; i < array.length; i++) {
				if(array[i].username === $scope.account.username) {
					index = i;
					break;
				}
			}
			$scope.accounts.splice(index, 1);	
			$scope.accounts.push($scope.account);
			$scope.account = {type:"ADMIN"};
		};
		
		$scope.changeState = function(username) {
			bookservice.changeState(username).then(onChange, onError);
		};
		

		$scope.refresh = function() {
			$scope.error = false;
		};
		
};

app.controller("AdminController", AdminController);
}());