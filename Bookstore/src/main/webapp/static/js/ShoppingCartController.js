(function(){
var home = angular.module("bookshop");

var ShoppingCartController = function($scope, $http, $location, storeservice) {
	
	$scope.Range = function(start, end) {
	    var result = [];
	    for (var i = start; i <= 10; i++) {
	        result.push(i);
	        if (i == end)
	        	break;
	    }
	    return result;
	};
	    
	
	$("#datepick input").datepicker({
		todayHighlight: true,
		dateFormat: "yy-mm-dd"
	});
	
	
	$scope.searchBook = function(details) {
		$location.path("/books/results/" + details);
	};
	
	
	$scope.viewBook = function(bookId){
		$location.path("/books/item/" + bookId);
	};	
	
	
	var onOrderFound = function(response) {
		$scope.order = response.data;
	};
	
	var onBooksFound = function(response) {
		$scope.books = response.data;
	};
	
	var onPrincipalFound = function(data) {
		$scope.account = data;
		$http.get("/findBooksByOrder?username=" + $scope.account.username).then(onBooksFound);
		$http.get("/findActiveOrder?username=" + $scope.account.username).then(onOrderFound);
	};
	
	storeservice.getPrincipal().then(onPrincipalFound);	
	
	
	var onOrderUpdated = function(response) {
		$scope.order = response.data;	
		$http.get("/findBooksByOrder?username=" + $scope.account.username).then(onBooksFound);
	};
	
	var onError = function(reason) {
		$scope.error = reason.data.message;
	};
	
	$scope.updateOrder = function(bookId, quantity) {
		$scope.order = {username: $scope.account.username, bookId: bookId, quantity: quantity};
		$http.put("/updateOrder", $scope.order).then(onOrderUpdated, onError);
	};
	
	
	var onBookRemoved = function(response, bookId) {
		$scope.order = response.data;
		var index = -1;		
		var array = eval ($scope.books);
		for (var i = 0; i < array.length; i++) {
			if(array[i].book.id === bookId) {
				index = i;
				break;
			}
		}
		$scope.books.splice(index, 1);			
	};
	
	$scope.removeBook = function(bookId) {
		$scope.order = {username: $scope.account.username, bookId: bookId};
		$http.put("/removeBookFromOrder", $scope.order).then(function(response){onBookRemoved(response, bookId)}, onError);
	};
	
	
	var onOrderPaid = function(data) {
		$scope.ServerResponse = data;
		$scope.error = false;
		$scope.books = {};
		$scope.order = {};
		$("#myOrder").modal("hide");
	};
	
	$scope.buy = function() {
		$scope.payment.username = $scope.account.username;
		$http.post("/addCard", $scope.payment).then(onOrderPaid, onError);
	};
	
	$scope.refresh = function() {
		$scope.error = false;
	};
	
};

home.controller("ShoppingCartController", ShoppingCartController);
}());