(function(){
var home = angular.module("bookshop");

var HomeController = function($scope, $http, $location, storeservice) {
	
	var onList = function(data) {
		$scope.categories = data;
	}
	
	storeservice.getCategories().then(onList);
		
	
	$scope.findBooks = function(categoryName){
		$location.path("/books/" + categoryName);
	};
	
		
	var onFound = function(response) {
		$scope.books = response.data;
	};
	
	$http.get("/findNewestBooks").then(onFound);
	
	
	$scope.viewBook = function(bookId){
		$location.path("/books/item/" + bookId);
	};	
	
	
	$scope.searchBook = function(details) {
		$location.path("/books/results/" + details);
	};
	
	
	var onPrincipalFound = function(data) {
		$scope.account = data;
	};
	
	storeservice.getPrincipal().then(onPrincipalFound);
	
	
	var onBookOrdered = function(data) {
		$scope.order = data;
	};
	
	var onError = function(reason) {
		$scope.error = reason.data.message;
	};
	
	$scope.addToCart = function(bookId) {
		$scope.book = {username: $scope.account.username, bookId: bookId};
		storeservice.addBook($scope.book).then(onBookOrdered, onError);
	};
	
};

home.controller("HomeController", HomeController);
}());