(function(){
var app = angular.module("bookstore");

var OrderController = function($scope, $http) {
	
	var onListComplete = function(response) {
		$scope.orders = response.data;
	};
	
	$http.get("/listOrders").then(onListComplete);
		
	
	
	var onOptionComplete = function(response) {
		$scope.option = response.data;
	};
	
	$scope.findOption = function(orderNumber) {
		$http.get("/findOption?orderNumber=" + orderNumber).then(onOptionComplete);
	};
	
};

app.controller("OrderController", OrderController);
}());