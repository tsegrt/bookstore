(function(){
var home = angular.module("bookshop",["ngRoute","datatables"]);
	
home.config(function($routeProvider){
	$routeProvider.when("/",{
		templateUrl:"/static/templates/user.html",
		controller:"HomeController"
	}).when("/accountDetails", {
		templateUrl:"/static/templates/accountDetails.html",
		controller:"AccountDetailsController"
	}).when("/contactInfo", {
		templateUrl:"/static/templates/contactInfo.html",
		controller:"ContactInfoController"
	}).when("/orderHistory", {
		templateUrl:"/static/templates/orderHistory.html",
		controller:"OrderHistoryController"
	}).when("/wishlist", {
		templateUrl:"/static/templates/wishlist.html",
		controller:"WishlistController"
	}).when("/bestsellers", {
		templateUrl:"/static/templates/bestsellers.html",
		controller:"BestsellersController"
	}).when("/shoppingCart", {
		templateUrl:"/static/templates/shoppingCart.html",
		controller:"ShoppingCartController"
	}).when("/books/:categoryName", {
		templateUrl:"/static/templates/bookList.html",
		controller:"BooklistController"
	}).when("/books/item/:bookId", {
		templateUrl:"/static/templates/bookItem.html",
		controller:"BookItemController"
	}).when("/books/results/:details", {
		templateUrl:"/static/templates/bookList.html",
		controller:"SearchController"
	});

});

home.filter("pagination", function() {
	return function(input, start) {
		if(!input || !input.length) {
			return;
		}
		start = +start;
		return input.slice(start);
	};
});
	
}());