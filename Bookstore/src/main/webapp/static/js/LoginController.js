(function(){
	var app = angular.module("logger", []);
	
	app.controller("LoginController", function($scope, $http) {
		
		var onUserRegistered = function(response, contact){
			$scope.user = response.data;
			$scope.contact.accountId = $scope.user.id;
			$http.put("/editContactInfo", $scope.contact).then(onContactAdded);
		};
		
		var onError = function(reason) {
			$scope.error = reason.data.message;
		};
		
		var onContactAdded = function(data){
			$scope.ServerResponse = data;
			$("#myModal").modal("hide");
		};
		
		$scope.registerUser = function(){
			$scope.user.type = "USER";
			console.log($scope.user);
			$http.post("/newUser", $scope.user).then(function(response){onUserRegistered(response,$scope.contact)},onError);
		};
		
		$scope.refresh = function(){
			$scope.error = false;
		};
		
		
	});
}());
