(function(){
var app = angular.module("bookstore");

var CustomerController = function($scope, $http, bookservice) {
	
	var onUserComplete = function (response) {
		$scope.users = response.data;
	};
	
	$http.get("/listAccounts?type=USER").then(onUserComplete);
	
	
	var onOrders = function(data) {
		$scope.orders = data;
	};
	
	var onError = function(reason) {
		$scope.error = reason.data.message;
	};
	
	$scope.findOrders = function(userId) {
		bookservice.getOrders(userId).then(onOrders, onError);
	};
	

	var onUserFound = function(response) {
		$scope.user = response.data;
	}
	
	$scope.findUser = function(username) {
		$http.get("/findAccountByUsername?username=" + username).then(onUserFound, onError);
	}
	
	
	var onUserDelete = function(data) {
		$scope.ServerResponse = data;
		var index = -1;		
		var array = eval ($scope.users);
		for (var i = 0; i < array.length; i++) {
			if(array[i].username === $scope.user.username) {
				index = i;
				break;
			}
		}
		$scope.users.splice(index, 1);	
		$("#deleteUser").modal("hide");
	};
	
	$scope.deleteUser = function(username) {
		$http["delete"]("/deleteAccount?username=" + username).then(onUserDelete, onError);
	};

	
	var onContactFound = function(response) {
		$scope.contact = response.data;
	};
	
	$scope.findContact = function(userId) {
		$http.get("/findContactInfo?accountId=" + userId).then(onContactFound, onError);
	};
	
	
	var onChange = function(data){
		$scope.user = data
		var index = -1;		
		var array = eval ($scope.users);
		for (var i = 0; i < array.length; i++) {
			if(array[i].username === $scope.user.username) {
				index = i;
				break;
			}
		}
		$scope.users.splice(index, 1);	
		$scope.users.push($scope.user);
		$scope.user = {};
	};
	
	$scope.changeState = function(username) {
		bookservice.changeState(username).then(onChange, onError);
	};

};

app.controller("CustomerController", CustomerController);
}());