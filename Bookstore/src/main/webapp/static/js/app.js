(function(){
var app = angular.module("bookstore",["ngRoute","datatables"]);
	
app.config(function($routeProvider){
	$routeProvider.when("/",{
		templateUrl:"/static/templates/admin.html",
		controller:"AdminController"
	}).when("/customers", {
		templateUrl:"/static/templates/customers.html",
		controller:"CustomerController"
	}).when("/orders", {
		templateUrl:"/static/templates/orders.html",
		controller:"OrderController"
	}).when("/categories", {
		templateUrl:"/static/templates/categories.html",
		controller:"CategoryController"
	}).when("/books", {
		templateUrl:"/static/templates/books.html",
		controller:"BookController"
	});
});
	
}());