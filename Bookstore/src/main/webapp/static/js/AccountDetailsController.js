(function(){
var home = angular.module("bookshop");

var AccountDetailsController = function($scope, $http, storeservice) {
		
		var onUserFound = function(data) {
			$scope.user = data;
		};
	
		var onPrincipalFound = function (data) {
			$scope.account = data;
			storeservice.getUser($scope.account.username).then(onUserFound)
		};
		
		storeservice.getPrincipal().then(onPrincipalFound);

		
		var onEmailChange = function(response) {
			$scope.user = response.data;
			$scope.error = false;
			$("#myAccount").modal("show");
		};
		
		var onError = function(reason) {
			$scope.error = reason.data.message;
		};
		
		$scope.changeEmail = function() {
			$http.put("/updateEmail", $scope.user).then(onEmailChange, onError);
		};
		
		
		var onPasswordChange = function(response) {
			$scope.user = response.data;
			$scope.passerror = false;
			$("#myAccount").modal("show");
		};
		
		var onPassError = function(reason) {
			$scope.passerror = reason.data.message;
		};
		
		$scope.changePassword = function(){
			$http.put("/updatePassword", $scope.user).then(onPasswordChange, onPassError);
		};
		
		
		$scope.searchBook = function(details) {
			$location.path("/books/results/"+details);
		};
		
		
		$scope.refresh = function() {
			$scope.error = false;
		};
		
		$scope.reset = function() {
			$scope.passerror = false;
		};
};

home.controller("AccountDetailsController", AccountDetailsController);
}());