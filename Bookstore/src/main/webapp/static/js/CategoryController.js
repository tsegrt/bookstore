(function(){
var app = angular.module("bookstore");

var CategoryController = function($scope, $http, bookservice) {

	var onListComplete = function(data) {
		$scope.categories = data;
	};
	
	bookservice.getCategories().then(onListComplete);
	

	var onCategoryFound = function(response) {
		$scope.category = response.data;
	};
	
	var onError = function(reason) {
		$scope.error = reason.data.message;
	};
	
	$scope.findCategory = function(categoryName) {
		$http.get("/findCategory?name=" + categoryName).then(onCategoryFound, onError);
	};
	
	
	var onSuccess = function(response) {
		$scope.category = response.data
		var index = -1;		
		var array = eval ($scope.categories);
		for (var i = 0; i < array.length; i++) {
			if(array[i].id === $scope.category.id) {
				index = i;
				break;
			}
		}
		$scope.categories.splice(index, 1);	
		$scope.categories.push($scope.category)
		$scope.category = {};
		$("#myCategory").modal("hide");
	};
	
	$scope.rename = function() {
		$http.put("/renameCategory", $scope.category).then(onSuccess, onError);
	};
	
	
	var onCreated = function(response) {
		$scope.category = response.data;
		$scope.categories.push($scope.category);
		$scope.category = {};
		$("#newCategory").modal("hide");
	};
	
	$scope.newCategory = function() {
		$http.post("/addCategory", $scope.category).then(onCreated, onError);
	};
	

	var onBooksFound = function(response) {
		$scope.books = response.data;
	};
	
	$scope.findBooks = function(categoryName) {
		$http.get("/findBooksByCategory?categoryName=" + categoryName).then(onBooksFound, onError);
	};
	
	
	var onRemoved = function(data) {
		$scope.ServerResponse = data;
		var index = -1;		
		var array = eval($scope.categories);
		for (var i = 0; i < array.length; i++) {
			if (array[i].name === $scope.category.name) {
				index = i;
				break;
			}
		}
		$scope.categories.splice(index, 1);	
		$("#removeCategory").modal("hide");
	};
	
	$scope.removeCategory = function(categoryName) {
		$http["delete"]("/removeCategory?name=" + categoryName).then(onRemoved, onError);
	};
	
};

app.controller("CategoryController", CategoryController);
}());