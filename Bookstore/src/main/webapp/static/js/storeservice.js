(function(){
	
	var storeservice = function($http) {

		var getCategories = function() {
			return $http.get("/listCategories")
						.then(function(response){
						 return response.data;
			});
		};
		
		var getBooksByCategory = function(categoryName) {
			return $http.get("/findBooksByCategory?categoryName="+categoryName)
						.then(function(response){
						 return response.data;
			});
		};
		
		var getBooks = function() {
			return $http.get("/listBooks")
						.then(function(response){
						 return response.data;
			});
		};
		
		var getPrincipal = function() {
			return $http.get("/findPrincipal")
						.then(function(response){
						 return response.data;
			});
		};
		
		var getUser = function(username) {
			return $http.get("/findAccountByUsername?username=" + username)
						.then(function(response){
						 return response.data;
			});
		};
		
		var addBook = function(book) {
			return $http.put("/addBookToOrder", book)
						.then(function(response){
						return response.data;
			});
		};

		return {
			getCategories: getCategories,
			getBooksByCategory: getBooksByCategory,
			getBooks: getBooks,
			getPrincipal: getPrincipal,
			getUser: getUser,
			addBook : addBook
		};
						
		
	};
	
	var module = angular.module("bookshop");
	module.factory("storeservice", storeservice);
	
}());