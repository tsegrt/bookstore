(function(){
	
	var bookservice = function($http) {
		
		var getOrders = function (user) {
			return $http.get("/findOrdersByUser?accountId=" + user)
						.then(function(response){
						 return response.data;
			});
			
		};
		
		var getCategories = function() {
			return $http.get("/listCategories")
						.then(function(response){
						 return response.data;
			});
		};
		
		var changeState = function(username) {
			return $http.put("/updateState?username=" + username)
						.then(function(response){
							return response.data;
						});
		};
		
		
		return {
			getOrders: getOrders,
			getCategories: getCategories,
			changeState: changeState
		};
						
		
	};
	
	var module = angular.module("bookstore");
	module.factory("bookservice", bookservice);
	
}());
