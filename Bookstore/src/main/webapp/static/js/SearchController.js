(function(){
var home = angular.module("bookshop");

var SearchController = function($scope, $http, $location, $routeParams, storeservice) {
	
	$scope.currentPage = 0;
	$scope.pageSize = 6;
	
	var onList = function(data) {
		$scope.categories = data;
	};
	
	storeservice.getCategories().then(onList);

	$scope.sortOrder="-soldItems";

	var onBookFound = function(response) {
		$scope.books = response.data;
		if($scope.books.length > 0) {
			$scope.numberOfPages = Math.ceil($scope.books.length / $scope.pageSize);
		}
		else {
			$scope.numberOfPages = 1;
		}
	}
	
	var onError = function(reason) {
		$scope.error = reason.data.message;
	};
	
	$scope.details = $routeParams.details
	$http.get("/searchBooks?details=" + $scope.details).then(onBookFound);
	
	
	$scope.searchBook = function(details) {
		$location.path("/books/results/"+details);
	}
	
	
	
	$scope.findBooks = function(categoryName){
		$location.path("/books/"+categoryName);
	};
	
	
	
	$scope.viewBook = function(bookId){
		$location.path("/books/item/"+bookId);
	};	


	
	var onPrincipalFound = function(data) {
		$scope.account = data;
	};
	
	storeservice.getPrincipal().then(onPrincipalFound);
	
	
	
	var onBookOrdered = function(data) {
		$scope.order = data;
	};
	
	$scope.addToCart = function(bookId) {
		$scope.book={username: $scope.account.username, bookId: bookId};
		storeservice.addBook($scope.book).then(onBookOrdered, onError);
	};

	
	
	
	$scope.maxprice="10000000000";

	$scope.bookFilter = function (book) {
        return (book.price <= $scope.maxprice);
    };
    
    $scope.search = function(filter, length) {
    	$scope.maxprice = filter;
    	$scope.currentPage = 0;
		if(length > 0) {
			$scope.numberOfPages = Math.ceil(length / $scope.pageSize);
		}
		else {
			$scope.numberOfPages = 1;
		}
    }
		
};

home.controller("SearchController", SearchController);
}());