(function(){
var home = angular.module("bookshop");

var ContactInfoController = function($scope, $http, storeservice) {
	
	var onContactFound = function(response) {
		$scope.contact = response.data;
	};
	
	var onUserFound = function(data) {
		$scope.user = data;
		$http.get("/findContactInfo?accountId=" + $scope.user.id).then(onContactFound);
	};

	var onPrincipalFound = function (data) {
		$scope.account = data;
		storeservice.getUser($scope.account.username).then(onUserFound)
	};
	
	storeservice.getPrincipal().then(onPrincipalFound);
	
	
	var onContactChanged = function(response) {
		$scope.contact = response.data;
		$("#myContact").modal("show");
	};
	
	var onError = function(reason) {
		$scope.error = reason.data.message;
	};
	
	$scope.changeContact = function(accountId){
		$scope.contact.accountId = accountId;
		$http.put("/editContactInfo", $scope.contact).then(onContactChanged, onError);
	};
		
	
	$scope.searchBook = function(details) {
		$location.path("/books/results/"+details);
	};
	
};

home.controller("ContactInfoController", ContactInfoController);
}());