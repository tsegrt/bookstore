(function(){
var app = angular.module("bookstore");

var BookController = function($scope, $http, bookservice) {
	
	$("#datepick input").datepicker({
		todayHighlight: true,
		dateFormat: "yy-mm-dd"
	});
	
	$scope.error = false;
	
	var onListCompleted = function(response) {
		$scope.books = response.data;
	};
	
	$http.get("/listBooks").then(onListCompleted);
		
	
	var onBookFound = function(response) {
		$scope.book = response.data;
	};
	
	$scope.findBookById = function(bookId) {
		$http.get("/findBookById?bookId=" + bookId).then(onBookFound, onError);
	};
	
	
	var onDescription = function(data) {
		$scope.ServerResponse = data;
		$("#myBook").modal("hide");
	};
	
	var onError = function(reason) {
		$scope.error = reason.data.message;
	};
	
	$scope.changeDescription = function() {
		$http.put("/changeBookDescription", $scope.book).then(onDescription, onError);
	};
	
	
	var onChange = function(response,id) {
		$scope.book = response.data;
		var index = -1;		
		var array = eval ($scope.books);
		for (var i = 0; i < array.length; i++) {
			if(array[i].isbn === $scope.book.isbn) {
				index = i;
				break;
			}
		}
		$scope.books.splice(index, 1);	
		$scope.books.push($scope.book)
		$scope.book = {};
		$(id).modal("hide");
		
		
	};
	
	$scope.changePrice = function() {
		$scope.id = "#myPrice"
		$http.put("/changeBookPrice", $scope.book).then(function(response){onChange(response, $scope.id)}, onError);
	};
	
	
	$scope.changeCategory = function() {
		$scope.book.categoryName = $scope.book.category.name;
		$scope.id = "#myCategory";
		$http.put("/changeBookCategory", $scope.book).then(function(response){onChange(response, $scope.id)}, onError);
	};
	
	
	$scope.changeQuantity = function() {
		$scope.id="#myQuantity";
		$http.put("/changeBookAvailability", $scope.book).then(function(response){onChange(response, $scope.id)}, onError);
	};
	
	
	var onDelete = function(data) {
		$scope.ServerResponse = data;
		var index = -1;		
		var array = eval ($scope.books);
		for (var i = 0; i < array.length; i++) {
			if(array[i].isbn === $scope.book.isbn) {
				index = i;
				break;
			}
		}
		$scope.books.splice(index, 1);	
		$("#deleteBook").modal("hide");
	};
	
	$scope.deleteBook = function(bookId) {
		$http["delete"]("/removeBook?bookId=" + bookId).then(onDelete, onError);
	};
	
	
	var onListComplete = function(data) {
		$scope.categories = data;
	};
	
	bookservice.getCategories().then(onListComplete, onError);
	
	
	var onBookCreated = function(response) {
		$scope.book = response.data;
		$scope.books.push($scope.book);
		$scope.book = {};
		$scope.error = false;
		$("#addBook").modal("hide");
	};
	
	$scope.addBook = function() {
		$http.post("/newBook", $scope.book).then(onBookCreated, onError);
	};

	
	$scope.reset = function() {
		$scope.book = {"category": $scope.categories[0].id};
	};
	
	
	$scope.refresh = function() {
		$scope.error = false;
	};
};

app.controller("BookController", BookController);
}());